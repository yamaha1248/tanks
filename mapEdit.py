import json

import pygame
import pygame_menu
import keyboard
import mouse
import colors

from map import Dynamite, Armor, Brick

class TankGame:
    def __init__(self):
        pygame.mixer.pre_init(44100, -16, 1, 512)  # важно прописать до pygame.init()
        pygame.init()
        pygame.mixer.music.load("sounds/back2.mp3")
        pygame.mixer.music.play(-1)
        pygame.mixer.music.set_volume(0.3)

        self.FPS = 30
        self.cell_size = 32
        self.x_cells = 37
        self.y_cells = 28
        self.timer = 13500

        self.sc = pygame.display.set_mode((self.x_cells * (self.cell_size + 20), self.y_cells * (self.cell_size + 4)))
        pygame.display.set_caption('редактор карт')
        self.clock = pygame.time.Clock()

        self.all_sprites = pygame.sprite.Group()
        self.all_not_tnt = pygame.sprite.Group()
        self.all_missiles = pygame.sprite.Group()
        self.all_bricks = pygame.sprite.Group()
        self.all_armors = pygame.sprite.Group()
        self.all_dynamite = pygame.sprite.Group()
        self.all_base1 = pygame.sprite.Group()
        self.all_base2 = pygame.sprite.Group()
        self.all_blocks = pygame.sprite.Group()
        self.all_bad = pygame.sprite.Group()
        self.all_obstacles = pygame.sprite.Group()
        self.all_bonus = pygame.sprite.Group()

        self.game_running = False
        self.map_list = []
        self.pos1 = (0, 0)
        self.pos2 = (0, 0)
        self.space_pos1 = (0, 0)
        self.space_pos2 = (0, 0)

        with open("settings/parameters.txt", "r") as file:
            d = json.load(file)
            m_lst = d.get("maps")
            for m in m_lst:
                self.map_list.append((m, m))
        self.map = "s"
        self.menu = self.createMenu()

    def pressProcessing(self):
        def killAll():
            for tank in self.all_bad:
                tank.kill()
            for brick in self.all_bricks:
                brick.kill()
            for armor in self.all_armors:
                armor.kill()
            for bonus in self.all_bonus:
                bonus.kill()
            for missile in self.all_missiles:
                missile.kill()
            for dynamite in self.all_dynamite:
                dynamite.kill()

        if keyboard.is_pressed("ctrl + q"):
            self.saveMap()

        if keyboard.is_pressed('s + h'):
            killAll()
            self.game_running = False

        if keyboard.is_pressed('delete'):
            killAll()

    def startTheGame(self):
        self.game_running = True

    def createMenu(self):
        menu = pygame_menu.Menu("Магазин", self.x_cells * (self.cell_size + 20), self.y_cells * (self.cell_size + 4),
                                theme=pygame_menu.themes.THEME_BLUE)
        menu.add.selector('карта', self.map_list, onchange=self.chooseMap)
        menu.add.button('старт', self.startTheGame)
        return menu

    def createMap(self):
        try:
            with open('maps/' + self.map + '_blocks.txt', "r") as json_file:
                blocks = json.load(json_file)
                for xy in blocks.get("armors"):
                    Armor(self, xy)
                for xy in blocks.get("bricks"):
                    Brick(self, xy)
                for xy in blocks.get("dynamites"):
                    Dynamite(self, xy)

            print("карта загружена")
        except FileNotFoundError:
            print("такой карты нет")
            self.createMap()

    def drawGrid(self):
        for x in range(self.x_cells):
            for y in range(self.y_cells + 1):
                cell = pygame.Rect(x * self.cell_size, y * self.cell_size, self.cell_size, self.cell_size)
                pygame.draw.rect(self.sc, colors.WHITE, cell, 1)

    def chooseMap(self, value, x):
        self.map = value[0][1]

    def playGame(self, events):
        if mouse.is_pressed(mouse.LEFT):
            for s in self.all_sprites:
                if s.rect.collidepoint(pygame.mouse.get_pos()):
                    self.pos1 = s.rect.topleft
                    print(type(self.pos1))
        if mouse.is_pressed(mouse.RIGHT):
            for s in self.all_sprites:
                if s.rect.collidepoint(pygame.mouse.get_pos()):
                    self.pos2 = s.rect.topleft

        self.pressProcessing()
        self.sc.fill(colors.BLACK)
        self.all_sprites.draw(self.sc)
        self.all_sprites.update(events=events)
        self.updateEdit()

        # drawGrid()

    def saveMap(self):
        name = input("Введите назвение карты: ")
        self.map_list.append((name, name))
        bricks = []
        for brick in self.all_bricks:
            bricks.append((brick.rect.topleft[0] // self.cell_size, brick.rect.topleft[1] // self.cell_size))

        armors = []
        for armor in self.all_armors:
            armors.append((armor.rect.topleft[0] // self.cell_size, armor.rect.topleft[1] // self.cell_size))

        dynamites = []
        for dynamite in self.all_dynamite:
            dynamites.append((dynamite.rect.topleft[0], dynamite.rect.topleft[1]))

        blocks_dict = {"armors": armors, "bricks": bricks, "dynamites": dynamites}

        with open("maps/" + name + '_blocks.txt', "w") as file:
            json.dump(blocks_dict, file)
        with open("settings/parameters.txt", "w") as map_file:
            m_lst = []
            for m in self.map_list:
                m_lst.append(m[0])
            json.dump({"maps": list(set(m_lst))}, map_file)
        print("Карта сохранена")

    def PrintParameters(self):
        font = pygame.font.SysFont("Segoe UI", self.cell_size - 5)
        fontMap = pygame.font.SysFont("Segoe UI", self.cell_size * 2)

        pos1_render = font.render(f"pos1: {self.pos1}", True, colors.RED)
        self.sc.blit(pos1_render, (self.cell_size * 32, self.cell_size * 1))

        pos2_render = font.render(f"pos2: {self.pos2}", True, colors.RED)
        self.sc.blit(pos2_render, (self.cell_size * 32, self.cell_size * 2))

        space_pos1_render = font.render(f"fieldPos1: {self.space_pos1}", True, colors.RED)
        self.sc.blit(space_pos1_render, (self.cell_size * 32, self.cell_size * 3))

        space_pos2_render = font.render(f"fieldPos2: {self.space_pos2}", True, colors.RED)
        self.sc.blit(space_pos2_render, (self.cell_size * 32, self.cell_size * 4))


        map_render = fontMap.render(f"Map: {self.map}", True, colors.BLUE)
        self.sc.blit(map_render, (self.cell_size * 1, self.cell_size * 1))

    def getField(self):
        self.space_pos1 = (min(self.pos1[0], self.pos2[0]), min(self.pos1[1], self.pos2[1]))
        self.space_pos2 = (max(self.pos1[0], self.pos2[0]) + self.cell_size, max(self.pos1[1], self.pos2[1]) + self.cell_size)

        # pos1 = pygame.Rect(self.pos1[0], self.pos1[1], self.cell_size, self.cell_size)
        # pygame.draw.rect(self.sc, colors.GREEN, pos1, 2)
        #
        # pos2 = pygame.Rect(self.pos2[0], self.pos2[1], self.cell_size, self.cell_size)
        # pygame.draw.rect(self.sc, colors.YELLOW, pos2, 2)

        edit_rect = pygame.Rect(self.space_pos1[0], self.space_pos1[1], (self.space_pos2[0] - self.space_pos1[0]),
                            (self.space_pos2[1] - self.space_pos1[1]))
        pygame.draw.rect(self.sc, colors.GREEN, edit_rect, 3)

    def mapMaking(self):
        if keyboard.is_pressed("a"):
            for s in self.all_sprites:
                for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                    for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                        if s.rect.topleft == (i, j):
                            s.kill()
            for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                    Armor(self, (i / self.cell_size, j / self.cell_size))

        if keyboard.is_pressed("x"):
            for s in self.all_sprites:
                for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                    for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                        if s.rect.topleft == (i, j):
                            s.kill()
            for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                    Brick(self, (i / self.cell_size, j / self.cell_size))

        if keyboard.is_pressed("d"):
            for s in self.all_sprites:
                for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                    for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                        if s.rect.topleft == (i, j):
                            s.kill()
            for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                    Dynamite(self, (i, j))

        if keyboard.is_pressed("z"):
            for s in self.all_sprites:
                for i in range(self.space_pos1[0], self.space_pos2[0], self.cell_size):
                    for j in range(self.space_pos1[1], self.space_pos2[1], self.cell_size):
                        if s.rect.topleft == (i, j):
                            s.kill()

    def updateEdit(self):
        self.getField()
        self.mapMaking()
        self.PrintParameters()

    def main(self):
        fl = True
        while fl:
            self.events = pygame.event.get()
            for event in self.events:
                if event.type == pygame.QUIT:
                    fl = False
            if self.game_running:
                self.playGame(self.events)
            else:
                self.menu.update(self.events)
                self.menu.draw(self.sc)
                # self.events = None

                if keyboard.is_pressed("Tab"):
                    self.createMap()

            pygame.display.update()
            self.clock.tick(self.FPS)


if __name__ == '__main__':
    TankGame().main()

